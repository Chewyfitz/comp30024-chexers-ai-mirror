"""
montesim.py by Aidan Fitzpatrick (835833)

This file represents a single monte carlo simulation.

Tiles are represented in this function as hash values, and two hash functions are
defined below which can convert a given tile to its hash value.
Additionally, properties of the board are stored as precomputed dictionaries in
montedicts3.py (this is one of the reasons I'm using hash values, since it 
makes dictionary lookup a *lot* faster)
"""
import itertools
# import numpy as np
import random
from math import floor
# from Chexmate.montedicts import *
from Chexmate.montedicts3 import *

_ADJACENT_TILES = lambda a,b: {(a-1,b+0),(a+0,b-1),(a+1,b-1),(a+1,b+0),(a+0,b+1),(a-1,b+1)}
_ADJACENT_STEPS = [(-1,+0),(+0,-1),(+1,-1),(+1,+0),(+0,+1),(-1,+1)]
_BOARD_RANGE = range(-3,4)
_BOARD_AREA = [(q,r) for q in _BOARD_RANGE for r in _BOARD_RANGE if -q-r in _BOARD_RANGE]
_PLAYER_ENUM = {'my': 0, 'o1': 1, 'o2':2}
_MAX_TURNS = 256

# Note this _EXIT_MOVE varies from the one in montecarlo.py
# this is because (4, 4) has a reasonable hash value of 38, which does not
# collide with any other values, nor does it generate a negative hash value.
_EXIT_MOVE = (4, 4)
_EXIT_MOVE_HASHED = 38 # Also, defined this as the number because it's easier

pieces = [[],[],[]]

num_pcs = [4,4,4]

num_exited = [] 
o1_elim = False
o2_elim = False

# Basically just the goals list but hash values instead.
goals_lst_hashed = [
    {29, 30, 31, 32}, 
    {36, 28, 18, 6 },
    {33, 24, 13, 0 }
]

"""
    hash functions to map a given tile to its hash value.
    These functions can be replaced in the code with their definition for a
    faster execution time.
"""
f = [30,22,12,0,7,19,29,31]
hsh = lambda a,b: f[a+3]+b+3
hsh2 = lambda a: f[a[0]+3]+a[1]+3

move_num = 0

"""
    Simulation workhorse function
"""
def monte_carlo_sim(player):
    # self.my_pieces, self.opponent1_pieces, self.opponent2_pieces, self.goals, self.num_moves, self.num_exited
    global pieces
    global num_pcs

    global num_exited
    global o1_elim
    global o2_elim
    global goals_lst_hashed

    global validDict 
    global owned_by 
    global occupied 

    global move_num

    move_num = player.num_moves


    #First, initialise everything
    pieces[0] = [_EXIT_MOVE_HASHED] * 12
    pieces[1] = [_EXIT_MOVE_HASHED] * 12
    pieces[2] = [_EXIT_MOVE_HASHED] * 12

    num_exited = player.num_exited.copy()
    o1_elim = False
    o2_elim = False

    temp_goals = goals_lst_hashed.copy()
    if player.colour_abbr == 'g':
        # swap r and g goals
        temp_goals[0] = goals_lst_hashed[1] # r = g
        temp_goals[1] = goals_lst_hashed[0] # g = r
    elif player.colour_abbr == 'b':
        # swap r and g goals
        # swap g and b goals
        temp_goals[0] = goals_lst_hashed[2] # r = b
        temp_goals[1] = goals_lst_hashed[0] # g = r
        temp_goals[2] = goals_lst_hashed[1] # b = g

    goals_lst_hashed = temp_goals

    temp = [list(player.my_pieces),
            list(player.opponent1_pieces),
            list(player.opponent2_pieces)]
    num_pcs = [
        len(temp[0]),
        len(temp[1]),
        len(temp[2])
    ]

    # First set all pieces as occupied
    for pl in temp:
        for p in pl:
            occupied[hsh2(p)] = True
            owned_by[hsh2(p)] = p

    # Place all of the pieces in the board
    for i in range(12):
        if i < len(temp[0]):
            pieces[0][i] = hsh2(temp[0][i])
            place_tile(pieces[0][i], 0)
        else:
            pieces[0][i] = _EXIT_MOVE_HASHED
        if i < len(temp[1]):
            pieces[1][i] = hsh2(temp[1][i])
            place_tile(pieces[1][i], 1)
        else:
            pieces[1][i] = _EXIT_MOVE_HASHED
        if i < len(temp[2]):
            pieces[2][i] = hsh2(temp[2][i])
            place_tile(pieces[2][i], 2)
        else:
            pieces[2][i] = _EXIT_MOVE_HASHED
    # Now we only need to update owned_by and occupied as we go

    # Now move randomly until someone wins or the game ends
    # print("======================== LOOP ========================")
    for i in range(len(occupied)):
        if occupied[i]:
            if i not in pieces[owned_by[i]]:
                print("ERR... PIECE {} NOT OWNED".format(i))
                print("pieces: {}".format(pieces))
                print("occupied: {}".format(occupied))
                print("owned_by: {}".format(owned_by))
                exit()
    # print(move_num)
    rval = 0
    if num_pcs[0] < 1:
        print("*(pre-loop) num_pcs[0] < 1")
        return -1
    while num_exited[0] < 4:
        #Have each player make random moves until the game ends
        if num_pcs[0] < 1:
            # print("--0")
            rval = -1
            # print("num_pcs[0] < 1")
            break
        # print("======== 0 ========")
        # Check to see if we won
        if num_exited[0] == 4:
            # print("--1") #####
            break
        (pce, num) = move_randomly(0)
        move_num += 1
        if not num == 'PASS':
            apply_move(pce, num, 0)
        # Check if it's a draw
        if move_num > _MAX_TURNS:
            # print("--2")
            rval = num_exited[0]/4 -1
            # print("move_num > _MAX_TURNS")
            break


        # print("======== 1 ========")
        # The same as above for our opponents, 
        # except we can skip them if they die
        if num_pcs[1] < 1:
            o1_elim = True
        if not o1_elim:
            (pce, num) = move_randomly(1)
            if not num == 'PASS':
                apply_move(pce, num, 1)
        if num_exited[1] == 4:
            rval = -1
            # print("--3")
            # print("num_exited[1] == 4")
            break

        # print("======== 2 ========")
        if num_pcs[2] < 1:
            o2_elim = True
        if not o2_elim:
            (pce, num) = move_randomly(2)
            if not num == 'PASS':
                apply_move(pce, num, 2)
        if num_exited[2] == 4:
            rval = -1
            # print("--4")
            # print("num_exited[2] == 4")
            break

        # If both opponents are eliminated we'll probably win unless we run
        # of moves first (Not really worth calculating IMO)
        # if o1_elim and o2_elim:
            # print("--5")
            # rval = 1
            # break

    if num_exited[0] == 4:
        # print("--6")
        rval = 1
    ### Reset all the structures
    for i in range(len(validDict)):
        for x in validDict[i]:
            x = False
    for i in range(len(occupied)):
        occupied[i] = False
    for i in owned_by:
        owned_by[i] = -1
    # num_exited = [0,0,0]
    # print(move_num)
    return rval

"""
    place_tile and take_tile set the values of the occupied and owned_by
    dictionaries so that we have information about the current state of the 
    board.
"""
def place_tile(htile, p):
    global occupied
    global owned_by
    occupied[htile] = True
    owned_by[htile] = p

def take_tile(htile):
    global occupied
    global owned_by
    occupied[htile] = False
    owned_by[htile] = -1

"""
    Apply the move found in move_randomly to the board so the simulation can
    continue.
"""
prev_move = (0, 0, 0)
def apply_move(pce, num, p):
    global pieces
    global num_exited
    global num_pcs

    global prev_move

    old = pieces[p][pce]
    new = possibleDict[old][num]
    ijp = 0
    take_tile(old)
    if new == _EXIT_MOVE_HASHED:
        num_pcs[p] -= 1
        if pce == num_pcs[p]:
            # Set the end piece to _EXIT_MOVE
            pieces[p][pce] = new
        else: 
            # swap this piece to the end
            temp = pieces[p][num_pcs[p]]
            pieces[p][num_pcs[p]] = new
            pieces[p][pce] = temp
        num_exited[p] += 1
        prev_move = (pce, num, p)
        return
    d = jump_dict[old][new][0]
    if d:
        # JUMP move, find the jumped piece
        jp = jump_dict[old][new][1]
        # Look for who owns the jumped piece
        owner = owned_by[jp]
        if owner == -1:
            print("@@@ JUMP ERR. montesim.py OWNER ERR")
            print("{} | {} tried to jump {} | {} to get to {} | {}".format(old, reverse_hash[old], jp, reverse_hash[jp],new,reverse_hash[new]))
            print(reverse_hash[jp])
            print(owned_by[jp])
            print(p)
            print(jp)
            print(occupied[jp])
            print(pieces)
            print(occupied)
            print(num_pcs)
            tmp = []
            for i in range(len(occupied)):
                if occupied[i]:
                    tmp += [(i,owned_by[i])]
            print(tmp)
            print(pieces[prev_move[2]][prev_move[0]])
            exit()
        # Ignore yourself, since if you own the jumped piece you don't 
        # need to take it
        if owner != p:
            if jp not in pieces[owner]:
                print("@@@ JUMP ERR. montesim.py NOT FOUND {}".format(move_num))
                print("{} | {} tried to jump {} | {} to get to {} | {}".format(old, reverse_hash[old], jp, reverse_hash[jp],new,reverse_hash[new]))
                print("occupied[old] {}".format(occupied[old]))
                print("reverse_hash[jp] {}".format(reverse_hash[jp]))
                print("owned_by[jp] {}".format(owned_by[jp]))
                print("p {}".format(p))
                print("jp {}".format(jp))
                print("occupied[jp] {}".format(occupied[jp]))
                print("pieces {}".format(pieces))
                print("occupied {}".format(occupied))
                print("num_pcs {}".format(num_pcs))
                tmp = []
                for i in range(len(occupied)):
                    if occupied[i]:
                        tmp += [(i,owned_by[i])]
                print(tmp)
                print(pieces[prev_move[2]][prev_move[0]])
                exit()
            # First give us the piece.
            pieces[p][num_pcs[p]] = jp
            num_pcs[p] += 1
            owned_by[jp] = p
            # Then remove it from their pieces
            num_pcs[owner] -= 1
            # Swap the removed piece to the end
            if jp == pieces[owner][num_pcs[owner]]:
                pieces[owner][num_pcs[owner]] = _EXIT_MOVE_HASHED
            else:
                ijp = pieces[owner].index(jp)
                if ijp > num_pcs[owner]:
                    pieces[owner][ijp] = _EXIT_MOVE_HASHED
                else:
                    pieces[owner][ijp] = pieces[owner][num_pcs[owner]]
                    pieces[owner][num_pcs[owner]] = _EXIT_MOVE_HASHED
        # Place the piece in the board
        pieces[p][pce] = new
    else:
        pieces[p][pce] = new
    place_tile(new, p)
    prev_move = (pce, num, p)

"""
    Check if a MOVE or JUMP is available
"""
def move_available(pce, possible):
    if jump_dict[pce][possible][0]:
        jp = jump_dict[pce][possible][1]
        return occupied[jp] and not occupied[possible]
    else:
        return not occupied[possible]

"""
    Choose a random move that is available to a given player.
"""
pi = 0
ps = 0
i  = 0
s  = 0
first1 = True
first2 = True
pml = 0
pce = 0
def move_randomly(p):
    global pieces
    global ps
    global pi
    global s
    global i
    global first1
    global first2
    global pml
    global pce
    first1 = True
    first2 = True
    # if num_pcs[p] == 0:
    #     return (0, 'PASS')
    ps = int(random.random() * (num_pcs[p] - 1))
    pi = ps
    # if pieces[p][ps] == _EXIT_MOVE_HASHED:
    #     pi = 0
    #     first1 = False
    # try:
    #     pml = len(possibleDict[pieces[p][pi]]) # Possible Move Length
    # except:
    #     print(p)
    #     print(num_pcs)
    #     print(pieces)
    #     exit()
    while pi != ps or first1:
        if pi >= num_pcs[p]:
            # print("gt. num_pcs[{}]: {}, pi: {}, ps: {}".format(p,num_pcs[p],pi,ps))
            pi = 0
            continue
        if pieces[p][pi] == _EXIT_MOVE_HASHED:
            pi += 1
            first1 = False
            continue
        pce = pieces[p][pi]
        pml = len(possibleDict[pieces[p][pi]])
        # print("pce: {}, goals_lst_hashed[p]: {}".format(pce, goals_lst_hashed[p]))
        goal_tile = pce in goals_lst_hashed[p]
        s = int(random.random() * (pml - 1))
        first2 = True
        i = s
        if possibleDict[pce][s] == _EXIT_MOVE_HASHED:
            if goal_tile:
                print("GOAL")
                return (pi,s)
            else:
                # Goal tiles are at the end
                i = 0
                first2 = False
        while i != s or first2:
            if i >= pml:
                i = 0
                continue
            if possibleDict[pce][i] == _EXIT_MOVE_HASHED:
                if goal_tile:
                    # print("GOAL {} | {}".format(pce, pieces))
                    # print((pi, i))
                    return (pi, i)
                i += 1
                continue
            if move_available(pce, possibleDict[pce][i]):
                # print((pi, i))
                return (pi, i)
            i += 1
            first2 = False
        pi += 1
        first1 = False
    # print((0, 'PASS'))
    return (0, 'PASS')