import copy

class Board:

	# order of players
	order = {"red":0, "green":1, "blue":2}

	# unit movement of a piece
	unit_moves = [(0,-1), (0,1), (-1,0), (1,0), (-1,1), (1,-1)]

	# weight values of the selected  features:
	# exits, average distance to goals[minimum_distance],
	# number of pieces captured, number of pieces got captured,
	weights = [3, -1, 2, -2]

	# define the goal positions for each color piece
	goals = [[(3,-3), (3,-2), (3,-1), (3,0)],
			 [(-3,3), (-2,3), (-1,3), (0,3)],
			 [(-3,0), (-2,-1), (-1,-2), (0,-3)]]

	def __init__(self):
		"""
		This method is called to initiate the board class

		"""
		#initial the starting positions of the pieces
		self.positions = [[(-3,0), (-3,1), (-3,2), (-3,3)],
						  [(0,-3), (1,-3), (2,-3), (3,-3)],
						  [(0,3), (1,2), (2,1), (3,0)]]
		self.player = 0
		self.exited = [0,0,0]   # store the exit pieces
		self.cap = [0,0,0]      # store the pieces capured from others
		self.getcap = [0,0,0]   # store the pieces get capured by others
		self.nodes = []         # store the boards after actions are taken
		self.previousAct = None # store the previous action
		self.avg_Dis = [100, 100, 100]
		self.utilVector = [-100,-100,-100]


    # verify if is is a legal position
	def isValid(self, a, b):
		ran = range(-3, +3+1)
		hexes = {(q,r) for q in ran for r in ran if -q-r in ran}
		if (a,b) in hexes:
			return True
		return False

	# verify if the position is available
	def isAvailable(self, a, b):
		for i in range(3):
			if (a,b) in self.positions[i]:
				return False
		return True

	def CopyBoard(self):
		"""
		copy the current board
		"""
		bcopy = Board()
		bcopy.positions = copy.deepcopy(self.positions)
		bcopy.exited = list(self.exited)
		bcopy.cap = list(self.cap)
		bcopy.getcap = list(self.getcap)
		return bcopy


	def getUtil(self):
		"""
		This method will be called when recalulating the utility vector
		"""
		self.calcAD()
		for i in range(3):
			feature = [self.exited[i], self.avg_Dis[i], self.cap[i], self.getcap[i]]
			self.utilVector[i] = sum(list(map(lambda x,y:x*y, feature, self.weights)))
		return self.utilVector


	def calcAD(self):
		"""
		Calculating the average distance of piece to the closest goals
		"""
		for i in range(3):
			shortestDist = []
            #game won
			if self.exited[i] >= 4:
					self.avg_Dis[i] = 0
					continue
			for (x,y) in self.positions[i]:
				temp = 100
				for (r,q) in self.goals[i]:
					temp = min(temp, abs(x-r) + abs(y-q))
				shortestDist.append(temp)

			if shortestDist != []:
				shortestDist.sort()
				# number of pieces left on the board
				numLeft = len(shortestDist)
				if (4-self.exited[i]) >= numLeft:
					self.avg_Dis[i] = sum(shortestDist)/numLeft
				else:
					#if there are pieces left more than we need [to fullfill 4 exits]
					#only calculate the average distance of the shortest possible ones
					#that we need
					self.avg_Dis[i] = sum(shortestDist[0:(4-self.exited[i])])/(4-self.exited[i])
			else:
				self.avg_Dis[i] = 0

	def move(self):
		"""
		Getting all the valid MOVEs
		"""
		for (x,y) in self.positions[self.player]:
			for (r,q) in self.unit_moves:
                #checking availability and validity of the adjcent tiles
				adjA = self.isAvailable(x+r,y+q)
				adjV = self.isValid(x+r,y+q)
				if adjV and adjA:
					node = self.CopyBoard()
					posAfter = node.positions
					# replace old position with new one
					for i in range(len([self.player])):
						if (posAfter[self.player][i] == (x,y)):
						   posAfter[self.player][i] = (x+r,y+q)

					node.getUtil()
					node.previousAct = ("MOVE", ((x,y), (x+r,y+q)))
					self.nodes.append(node)


	def jump(self):
		"""
		Getting all the valid JUMPS
		"""
		for (x,y) in self.positions[self.player]:
			for (r,q) in self.unit_moves:
                #checking availability and validity of the adjcent tiles
				#and the JUMP target tiles
				adjA = self.isAvailable(x+r,y+q)
				adjV = self.isValid(x+r,y+q)
				jumpA = self.isAvailable(x+2*r,y+2*q)
				jumpV = self.isValid(x+2*r,y+2*q)
				if adjV and not adjA and jumpV and jumpA:
					node = self.CopyBoard()
					posAfter = node.positions
					# replace old position with new one
					for i in range(len([self.player])):
						if (posAfter[self.player][i] == (x,y)):
						   posAfter[self.player][i] = (x+2*r,y+2*q)

                    #handling capture
					if (x+r, y+q) not in posAfter[self.player]:
						for i in range(3):
							if (x+r, y+q) in posAfter[i]:
								posAfter[i].remove((x+r,y+q))
								node.getcap[i] += 1
								break
						posAfter[self.player].append((x+r,y+q))
						node.cap[self.player] += 1

					node.getUtil()
					node.previousAct = ("JUMP", ((x,y), (x+2*r,y+2*q)))
					self.nodes.append(node)

	# check for valid EXIT
	def exit(self):
		for (x,y) in self.positions[self.player]:
			if (x,y) in self.goals[self.player]:
				node = self.CopyBoard()
				posAfter = node.positions
				# remove the piece after exiting
				posAfter[self.player].remove((x,y))
				node.previousAct = ("EXIT", ((x,y)))
				node.exited[self.player] += 1
				node.getUtil()
				self.nodes.append(node)

	# PASS action
	def passAction(self):
		node = self.CopyBoard()
		node.avg_Dis = list(self.avg_Dis)
		node.utilVector = list(self.utilVector)
		node.previousAct = ("PASS", None)
		self.nodes.append(node)


	# generate the board after an action is taken
	def genNextBoard(self, player):
		self.player = player
		self.nodes = []
		self.exit()
		self.jump()
		self.move()
		if self.nodes == []:
			self.passAction()
		return self.nodes
