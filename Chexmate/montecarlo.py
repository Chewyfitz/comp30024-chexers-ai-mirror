"""
montecarlo.py by Aidan Fitzpatrick (835833)

This is the workhorse Monte Carlo agent. it calls montesim to perform simulations.
There are leftover functions in here from before I split them off into montesim,
    but hopefully you don't see these in the final version.
"""
import itertools
from datetime import datetime
import random
from math import floor
from Chexmate import montesim
import Chexmate.montedicts

random.seed(datetime.now())

# Some definitions (mostly stolen from game.py)
_STARTING_HEXES = {
    'r': {(-3,3), (-3,2), (-3,1), (-3,0)},
    'g': {(0,-3), (1,-3), (2,-3), (3,-3)},
    'b': {(3, 0), (2, 1), (1, 2), (0, 3)},
}
_FINISHING_HEXES = {
    'r': {(3,-3), (3,-2), (3,-1), (3,0)},
    'g': {(-3,3), (-2,3), (-1,3), (0,3)},
    'b': {(-3,0),(-2,-1),(-1,-2),(0,-3)},
}
_ABBR_MAP = {
    'red': 'r',
    'green': 'g',
    'blue': 'b',
}
_ADJACENT_TILES = lambda a,b: {(a-1,b+0),(a+0,b-1),(a+1,b-1),(a+1,b+0),(a+0,b+1),(a-1,b+1)}

_ADJACENT_STEPS = [(-1,+0),(+0,-1),(+1,-1),(+1,+0),(+0,+1),(-1,+1)]
#_ADJACENT_JUMPS = [(-2,+0),(+0,-2),(+2,-2),(+2,+0),(+0,+2),(-2,+2)]
_ADJACENT_STEPS_JUMPS = [((-1,+0),(-2,+0)),((+0,-1),(+0,-2)),((+1,-1),(+2,-2)),((+1,+0),(+2,+0)),((+0,+1),(+0,+2)),((-1,+1),(-2,+2))]

_MAX_TURNS = 256 # per player
_EXIT_MOVE = (-9,-9)

# Some dictionaries from montedicts to use for available_moves
possible_dict = Chexmate.montedicts.possibleDict
jump_dict = Chexmate.montedicts.jump_dict

_BOARD_RANGE = range(-3,4)
_BOARD_AREA = [(q,r) for q in _BOARD_RANGE for r in _BOARD_RANGE if -q-r in _BOARD_RANGE]
_BOARD_AREA_SET = set(_BOARD_AREA)

## NUMBER OF SIMULATIONS PER MOVE
#  NOTE: 100 simulations per move is very slow but more effective than 25
#  if you'd like to see montecarlo perform better, try running with:
# _SIMS_PER_MOVE = 100
_SIMS_PER_MOVE = 25


class DumbMonte:
    def __init__(self, colour):
        """
        This method is called once at the beginning of the game to initialise
        your player. You should use this opportunity to set up your own internal
        representation of the game state, and any other information about the 
        game state you would like to maintain for the duration of the game.

        The parameter colour will be a string representing the player your 
        program will play as (Red, Green or Blue). The value will be one of the 
        strings "red", "green", or "blue" correspondingly.
        """
        # TODO: Set up state representation.
        self.colour = colour
        self.num_moves = 0 # this will let us keep track of how many moves we
                           # need to do for monte carlo

        # Assume we're red, then check to see if we aren't.
        # my_pieces is our currently owned piece list, 
        # and other_pieces is all the rest of the pieces.
        self.colour_abbr = 'r'
        self.opp1_colour_abbr = 'g'
        self.opp2_colour_abbr = 'b'
        if colour == "green":
            self.colour_abbr = 'g'
            self.opp1_colour_abbr = 'r'
        elif colour == "blue":
            self.colour_abbr = 'b'
            self.opp1_colour_abbr = 'r'
            self.opp2_colour_abbr = 'g'

        # Define the initial locations and goal locations for each player
        self.my_pieces        = _STARTING_HEXES[self.colour_abbr]
        my_goal              = _FINISHING_HEXES[self.colour_abbr]

        self.opponent1_pieces = _STARTING_HEXES[self.opp1_colour_abbr]
        opponent1_goal       = _FINISHING_HEXES[self.opp1_colour_abbr]

        self.opponent2_pieces = _STARTING_HEXES[self.opp2_colour_abbr]
        opponent2_goal       = _FINISHING_HEXES[self.opp2_colour_abbr]

        self.goals = [my_goal, opponent1_goal, opponent2_goal]

        self.occupied = Chexmate.montedicts.occupied.copy()

        self.num_exited = [0, 0, 0]
        # Set the inital occupied tiles
        for c in _STARTING_HEXES:
            for x in list(_STARTING_HEXES[c]):
                self.occupied[x] = True

    def action(self):
        """
            Running Monte Carlo Simulations until the cows come home
        """
        # Online Search:
        # Run Monte Carlo Search on every available action for every piece
        moves = available_moves_3(self)
        if len(moves) == 0:
            return ("PASS", None)

        move_to_take_score = -12000000
        move_to_take = moves[0]
        for move in moves:
            [my_pcs, o1_pcs, o2_pcs] = apply_move(move, self.my_pieces, 
                self.opponent1_pieces, self.opponent2_pieces, self)
            # Perform Monte Carlo simulations
            temp_score = 0
            for i in range(_SIMS_PER_MOVE):
                temp_score += montesim.monte_carlo_sim(self)
            # Adjust the scores of the simulation so they are weighted correctly
            temp_score = temp_score/_SIMS_PER_MOVE
            print(temp_score)
            # Choose the best move
            if move_to_take_score < temp_score:
                move_to_take_score = temp_score
                move_to_take = move
        # Return the correct move
        if move_to_take[0] == "EXIT":
            return move_to_take
        return (move_to_take[0], (move_to_take[1], move_to_take[2]))

    def update(self, colour, action):
        """
        This implementation looks a bit gross, but it works pretty fast
        """
        if action[0] == "PASS":
            return
        #convert "action" into internal "move" type
        move = (action[0], action[1][0], action[1][1])

        if _ABBR_MAP[colour] == self.colour_abbr:
            #update my own board
            temp_player_pieces = self.my_pieces.copy()
            self.num_moves += 1
        elif _ABBR_MAP[colour] == self.opp1_colour_abbr:
            #update opp1's board
            temp_player_pieces = self.opponent1_pieces.copy()
        else:
            #update opp2's board
            temp_player_pieces = self.opponent2_pieces.copy()

        #Fist handle the EXIT action since it only has 2 arguments
        move_type = move[0]
        move_source = move[1]
        if move_type == "EXIT":
            self.occupied[(move[1], move[2])] = False # for available_moves
            temp_player_pieces.remove((move[1], move[2]))
            if _ABBR_MAP[colour] == self.colour_abbr:
                self.my_pieces = temp_player_pieces.copy()
            elif _ABBR_MAP[colour] == self.opp1_colour_abbr:
                self.opponent1_pieces = temp_player_pieces.copy()
            else:
                self.opponent2_pieces = temp_player_pieces.copy()
            return

        #If it isn't an EXIT then it has 3 arguments
        move_destination = move[2]
        if move_type == "MOVE":
            temp_player_pieces.remove(move_source)
            self.occupied[move_source] = False # for available_moves
            temp_player_pieces.add(move_destination)
            self.occupied[move_destination] = True # for available_moves
        elif move_type == "JUMP":
            #We need to find the piece that we jumped over
            taken_piece = (int((move_source[0] + move_destination[0])/2), 
                int((move_source[1] + move_destination[1])/2))
            if taken_piece in self.opponent1_pieces:
                self.opponent1_pieces.remove(taken_piece)
                temp_player_pieces.add(taken_piece)
            elif taken_piece in self.opponent2_pieces:
                self.opponent2_pieces.remove(taken_piece)
                temp_player_pieces.add(taken_piece)
            elif taken_piece in self.my_pieces:
                self.my_pieces.remove(taken_piece)
                temp_player_pieces.add(taken_piece)
            temp_player_pieces.remove(move_source)
            self.occupied[move_source] = False # for available_moves
            temp_player_pieces.add(move_destination)
            self.occupied[move_destination] = True # for available_moves

        if _ABBR_MAP[colour] == self.colour_abbr:
            #update my own board
            self.my_pieces = temp_player_pieces.copy()
        elif _ABBR_MAP[colour] == self.opp1_colour_abbr:
            #update opp1's board
            self.opponent1_pieces = temp_player_pieces.copy()
        else:
            #update opp2's board
            self.opponent2_pieces = temp_player_pieces.copy()

def apply_move(move, player_pieces, opponent1_pieces, opponent2_pieces, player):
    #Fist handle the EXIT action since it only has 2 arguments
    move_type = move[0]
    move_source = move[1]
    temp_player_pieces = player_pieces.copy()
    if move_type == "EXIT":
        temp_player_pieces.remove(move_source)
        return [temp_player_pieces, opponent1_pieces, opponent2_pieces]

    # If it isn't an EXIT then it has 3 arguments
    # note that every instance of 'PASS' is handled outside of this function.
    move_destination = move[2]
    opp1_pieces = opponent1_pieces
    opp2_pieces = opponent2_pieces
    if move_type == "MOVE":
        # remove the old piece, replace it with the new piece
        temp_player_pieces.remove(move_source)
        temp_player_pieces.add(move_destination)
    elif move_type == "JUMP":
        #We need to find the piece that we jumped over
        taken_piece = (int((move_source[0] + move_destination[0])/2), 
            int((move_source[1] + move_destination[1])/2))
        # check which player owns it
        if taken_piece in opponent1_pieces:
            opp1_pieces = opponent1_pieces.copy()
            opp1_pieces.remove(taken_piece)
            temp_player_pieces.add(taken_piece)
        elif taken_piece in opponent2_pieces:
            opp2_pieces = opponent2_pieces.copy()
            opp2_pieces.remove(taken_piece)
            temp_player_pieces.add(taken_piece)
        # if neither opponent owns it, we don't need to do anything
        # special.
        temp_player_pieces.remove(move_source)
        temp_player_pieces.add(move_destination)

    return [temp_player_pieces, opp1_pieces, opp2_pieces]

# Determine which moves are available for every piece owned by player
def available_moves_3(player):
    # print(player.occupied)
    possible_moves = []
    cJ = True
    for x in player.my_pieces:
        for mv in possible_dict[x]:
            cJ = True
            if jump_dict[x][mv][0]:
                if player.occupied[jump_dict[x][mv][1]]:
                    if not player.occupied[mv]:
                        possible_moves += [("JUMP", x, mv)]
            elif mv == _EXIT_MOVE:
                continue
            elif not player.occupied[mv]:
                possible_moves += [("MOVE", x, mv)]
        if x in _FINISHING_HEXES[player.colour_abbr]:
            possible_moves += [("EXIT", x)]
    return possible_moves