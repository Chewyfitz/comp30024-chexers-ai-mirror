from Chexmate.montesim import *
from Chexmate.montecarlo import *
import random

# Some definitions (mostly stolen from game.py)
_STARTING_HEXES = {
    'r': {(-3,3), (-3,2), (-3,1), (-3,0)},
    'g': {(0,-3), (1,-3), (2,-3), (3,-3)},
    'b': {(3, 0), (2, 1), (1, 2), (0, 3)},
}
_FINISHING_HEXES = {
    'r': {(3,-3), (3,-2), (3,-1), (3,0)},
    'g': {(-3,3), (-2,3), (-1,3), (0,3)},
    'b': {(-3,0),(-2,-1),(-1,-2),(0,-3)},
}
_ABBR_MAP = {
    'red': 'r',
    'green': 'g',
    'blue': 'b',
}
_ADJACENT_TILES = lambda a,b: {(a-1,b+0),(a+0,b-1),(a+1,b-1),(a+1,b+0),(a+0,b+1),(a-1,b+1)}

_ADJACENT_STEPS = [(-1,+0),(+0,-1),(+1,-1),(+1,+0),(+0,+1),(-1,+1)]
#_ADJACENT_JUMPS = [(-2,+0),(+0,-2),(+2,-2),(+2,+0),(+0,+2),(-2,+2)]
_ADJACENT_STEPS_JUMPS = [((-1,+0),(-2,+0)),((+0,-1),(+0,-2)),((+1,-1),(+2,-2)),((+1,+0),(+2,+0)),((+0,+1),(+0,+2)),((-1,+1),(-2,+2))]

_MAX_TURNS = 256 # per player
_EXIT_MOVE = (-9,-9)

possible_dict = Chexmate.montedicts.possibleDict
jump_dict = Chexmate.montedicts.jump_dict

_BOARD_RANGE = range(-3,4)
_BOARD_AREA = [(q,r) for q in _BOARD_RANGE for r in _BOARD_RANGE if -q-r in _BOARD_RANGE]
_BOARD_AREA_SET = set(_BOARD_AREA)

# class Rando:
class Player:
    def __init__(self, colour):
        ## The same as in montecarlo.py
        self.colour = colour
        self.num_moves = 0 
        self.colour_abbr = 'r'
        self.opp1_colour_abbr = 'g'
        self.opp2_colour_abbr = 'b'
        if colour == "green":
            self.colour_abbr = 'g'
            self.opp1_colour_abbr = 'r'
        elif colour == "blue":
            self.colour_abbr = 'b'
            self.opp1_colour_abbr = 'r'
            self.opp2_colour_abbr = 'g'

        self.my_pieces        = _STARTING_HEXES[self.colour_abbr]
        self.opponent1_pieces = _STARTING_HEXES[self.opp1_colour_abbr]
        self.opponent2_pieces = _STARTING_HEXES[self.opp2_colour_abbr]
        my_goal              = _FINISHING_HEXES[self.colour_abbr]
        opponent1_goal       = _FINISHING_HEXES[self.opp1_colour_abbr]
        opponent2_goal       = _FINISHING_HEXES[self.opp2_colour_abbr]

        self.goals = [my_goal, opponent1_goal, opponent2_goal]
        self.occupied = Chexmate.montedicts.occupied.copy()

        self.num_exited = [0, 0, 0]
        for c in _STARTING_HEXES:
            for x in list(_STARTING_HEXES[c]):
                self.occupied[x] = True

    def action(self):
        moves = available_moves_3(self)
        if len(moves) == 0:
            return ("PASS", None)
        n = int(random.random() * (len(moves) - 1))
        if len(moves[n]) < 3:
            return (moves[n][0], moves[n][1])
        # print(moves)
        print(self.my_pieces)
        try:
            return (moves[n][0], (moves[n][1], moves[n][2]))
        except:
            print(moves[n])
            return


    def update(self, colour, action):
        ## The same as in montecarlo.py
        if action[0] == "PASS":
            return
        move = (action[0], action[1][0], action[1][1])
        if _ABBR_MAP[colour] == self.colour_abbr:
            temp_player_pieces = self.my_pieces.copy()
            self.num_moves += 1
        elif _ABBR_MAP[colour] == self.opp1_colour_abbr:
            temp_player_pieces = self.opponent1_pieces.copy()
        else:
            temp_player_pieces = self.opponent2_pieces.copy()

        move_type = move[0]
        move_source = move[1]
        if move_type == "EXIT":
            self.occupied[(move[1], move[2])] = False # for available_moves_2
            temp_player_pieces.remove((move[1], move[2]))
            if _ABBR_MAP[colour] == self.colour_abbr:
                self.my_pieces = temp_player_pieces.copy()
            elif _ABBR_MAP[colour] == self.opp1_colour_abbr:
                self.opponent1_pieces = temp_player_pieces.copy()
            else:
                self.opponent2_pieces = temp_player_pieces.copy()
            return
        move_destination = move[2]
        if move_type == "MOVE":
            temp_player_pieces.remove(move_source)
            self.occupied[move_source] = False # for available_moves_2
            temp_player_pieces.add(move_destination)
            self.occupied[move_destination] = True # for available_moves_2
        elif move_type == "JUMP":
            taken_piece = (int((move_source[0] + move_destination[0])/2), 
                int((move_source[1] + move_destination[1])/2))
            if taken_piece in self.opponent1_pieces:
                self.opponent1_pieces.remove(taken_piece)
                temp_player_pieces.add(taken_piece)
            elif taken_piece in self.opponent2_pieces:
                self.opponent2_pieces.remove(taken_piece)
                temp_player_pieces.add(taken_piece)
            elif taken_piece in self.my_pieces:
                self.my_pieces.remove(taken_piece)
                temp_player_pieces.add(taken_piece)
            temp_player_pieces.remove(move_source)
            self.occupied[move_source] = False # for available_moves_2
            temp_player_pieces.add(move_destination)
            self.occupied[move_destination] = True # for available_moves_2

        if _ABBR_MAP[colour] == self.colour_abbr:
            self.my_pieces = temp_player_pieces.copy()
        elif _ABBR_MAP[colour] == self.opp1_colour_abbr:
            self.opponent1_pieces = temp_player_pieces.copy()
        else:
            self.opponent2_pieces = temp_player_pieces.copy()

def available_moves_3(player):
    possible_moves = []
    cJ = True
    for x in player.my_pieces:
        for mv in possible_dict[x]:
            cJ = True
            if jump_dict[x][mv][0]:
                if player.occupied[jump_dict[x][mv][1]]:
                    if not player.occupied[mv]:
                        possible_moves += [("JUMP", x, mv)]
            elif mv == _EXIT_MOVE:
                continue
            elif not player.occupied[mv]:
                possible_moves += [("MOVE", x, mv)]
        if x in _FINISHING_HEXES[player.colour_abbr]:
            possible_moves += [("EXIT", x)]
    return possible_moves
