from Chexmate2.board import Board

# player class
class Chexmate2Player:

    # order of players
    order = {"red":0, "green":1, "blue":2}

    def __init__(self, colour):
        """
        This method is called once at the beginning of the game to initialise
        your player. You should use this opportunity to set up your own internal
        representation of the game state, and any other information about the
        game state you would like to maintain for the duration of the game.
        The parameter colour will be a string representing the player your
        program will play as (Red, Green or Blue). The value will be one of the
        strings "red", "green", or "blue" correspondingly.
        """
        # set the colour and identifier for the player
        self.colour = colour
        self.pid = self.order[colour]
        self.board = Board()

    def action(self):
        """
        This method is called at the beginning of each of your turns to request
        a choice of action from your program.
        Based on the current state of the game, your player should select and
        return an allowed action to play on this turn. If there are no allowed
        actions, your player must return a pass instead. The action (or pass)
        must be represented based on the above instructions for representing
        actions.
        """
        # invoke maxN to get best action
        [util, action] = self.maxN(self.board, self.pid, 0)

        return action

    def update(self, colour, action):
        """
        This method is called at the end of every turn (including your player’s
        turns) to inform your player about the most recent action. You should
        use this opportunity to maintain your internal representation of the
        game state and any other information about the game you are storing.
        The parameter colour will be a string representing the player whose turn
        it is (Red, Green or Blue). The value will be one of the strings "red",
        "green", or "blue" correspondingly.
        The parameter action is a representation of the most recent action (or
        pass) conforming to the above in- structions for representing actions.
        You may assume that action will always correspond to an allowed action
        (or pass) for the player colour (your method does not need to validate
        the action/pass against the game rules).
        """
        # get the identifier of the player
        pid = self.order[colour]
        b = self.board
        p = b.positions
        # PASS action
        if action[0] == "PASS":
            return
        # EXIT action
        elif action[0] == "EXIT":
            p[pid].remove(action[1])
            b.exited[pid] += 1
        # MOVE action
        elif action[0] == "MOVE":
            p[pid].remove(action[1][0])
            p[pid].append(action[1][1])
        # JUMP action
        else:
            p[pid].remove(action[1][0])
            p[pid].append(action[1][1])
            jumpX = action[1][0][0] + int((action[1][1][0]-action[1][0][0])/2)
            jumpY = action[1][0][1] + int((action[1][1][1]-action[1][0][1])/2)
            if (jumpX,jumpY) not in p[pid]:
                for i in range(3):
                    if (jumpX,jumpY) in p[i]:
                        p[i].remove((jumpX,jumpY))
                        b.getcap[i] += 1
                        break
                p[pid].append((jumpX,jumpY))
                b.cap[pid] += 1

    def maxN(self, board, pid, counter):
        """
        Implementation of MaxN algorithm.
        Cutoff depth set to 3 to satisfy the time constraint
        This method will return the max utility vector and best action for the player
        When tie occurs, choose the relatively better action that can let the player have
        more advantage.
        """

        #making cutoff depth 3
        if(counter >= 3):
            return [board.getUtil(), board.previousAct]
        maxUtil = [-100, -100, -100]
        bestAction = ("PASS", None)
        nextBoard = board.genNextBoard(pid)

        for b in nextBoard:
                [util, action] = self.maxN(b, (pid+1)%3, counter+1)
                if util[pid] > maxUtil[pid]:
                    maxUtil = util
                    bestAction = b.previousAct
                elif util[pid] == maxUtil[pid]:
                    diffCurr = (maxUtil[pid]-maxUtil[(pid-1)%3]) + (maxUtil[pid]-maxUtil[(pid+1)%3])
                    diffNext = (util[pid]-util[(pid-1)%3]) + (util[pid]-util[(pid+1)%3])
                    if diffCurr < diffNext:
                       maxUtil = util
                       bestAction = b.previousAct
        return [maxUtil, bestAction]
