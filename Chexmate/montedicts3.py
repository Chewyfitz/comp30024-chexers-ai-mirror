"""
montedicts.py by Aidan Fitzpatrick (835833)

This file stores some information that is intrinsic to the board.
montesim.py uses this to determine if a given move is possible.

Dictionaries 3 and 4 store hashed values of the tiles (hashed using the function
f = [30,22,12,0,7,19,29,31]; hsh = lambda a,b: f[a+3]+b+3, which gives a unique 
integer for each tile on the board in the range 0-36)
"""


# A bunch of big Dictionaries to make board updates faster.

# No. 1 - Board Influence (influenceDict)
## How to Generate:
# _BOARD_RANGE = range(-3,4)
# _BOARD_AREA = [(q,r) for q in _BOARD_RANGE for r in _BOARD_RANGE if -q-r in _BOARD_RANGE]
# _ADJACENT_STEPS = [(-1,+0),(+0,-1),(+1,-1),(+1,+0),(+0,+1),(-1,+1)]
# f = [30,22,12,0,7,19,29,31]
# hsh = lambda a,b: f[a+3]+b+3
# move = {}
# for (a, b) in _BOARD_AREA:
#     move[hsh(a,b)] = ([],[],[])
#     for (x, y) in _ADJACENT_STEPS:
#         if (a + x, b + y) in _BOARD_AREA:
#             move[hsh(a,b)][0].append(hsh(a+x,b+y))
#             if (a - x, b - y) in _BOARD_AREA:
#                 move[hsh(a,b)][2].append((hsh(a+x,b+y),hsh(a-x,b-y)))
#         if (a+2*x, b+2*y) in _BOARD_AREA:
#             move[hsh(a,b)][1].append((hsh(a+x, b+y), hsh(a+2*x,b+2*y)))
"""
## Description:

This dictionary returns the moves that are influenced by a certain tile.
For example, tile (-3, 1) influences the moves which are available to:
Moves: (-3, 0), (-2, 0), (-2, 1), (-3, 2)
Jumps: (-1, -1), (-1, 1), (-3, 3)
Jumps over: (-3, 0), (-3, 2)

Note that the dictionary has three distinct move representations (subsections 
	of a given tile)
Type [0] is moves onto the input tile,
Type [1] is Jumps onto the input tile, and
Type [2] is Jumps across the input tile (This is important as it's easy to miss
		when you're thinking about updates)
"""

influenceDict = {
	33: (
			[24, 25, 34], 
			[(24, 13), (25, 15), (34, 35)], 
			[]
	), 
	34: (
			[33, 25, 26, 35], 
			[(25, 14), (26, 16), (35, 36)], 
			[(33, 35), (35, 33)]
	), 
	35: (
			[34, 26, 27, 36], 
			[(34, 33), (26, 15), (27, 17)], 
			[(34, 36), (36, 34)]
	), 
	36: (
			[35, 27, 28], 
			[(35, 34), (27, 16), (28, 18)], 
			[]
	), 
	24: (
			[13, 14, 25, 33], 
			[(13, 0), (14, 2), (25, 26)], 
			[(13, 33), (33, 13)]
	), 
	25: (
			[33, 24, 14, 15, 26, 34], 
			[(14, 1), (15, 3), (26, 27)], 
			[(33, 15), (24, 26), (14, 34), (15, 33), (26, 24), (34, 14)]
	), 
	26: (
			[34, 25, 15, 16, 27, 35], 
			[(25, 24), (15, 2), (16, 4), (27, 28)], 
			[(34, 16), (25, 27), (15, 35), (16, 34), (27, 25), (35, 15)]
	), 
	27: (
			[35, 26, 16, 17, 28, 36], 
			[(26, 25), (16, 3), (17, 5)], 
			[(35, 17), (26, 28), (16, 36), (17, 35), (28, 26), (36, 16)]
	), 
	28: (
			[36, 27, 17, 18], 
			[(27, 26), (17, 4), (18, 6)], 
			[(36, 18), (18, 36)]
	), 
	13: (
			[0, 1, 14, 24], 
			[(1, 8), (14, 15), (24, 33)], 
			[(0, 24), (24, 0)]
	), 
	14: (
			[24, 13, 1, 2, 15, 25], 
			[(1, 7), (2, 9), (15, 16), (25, 34)], 
			[(24, 2), (13, 15), (1, 25), (2, 24), (15, 13), (25, 1)]
	), 
	15: (
			[25, 14, 2, 3, 16, 26], 
			[(25, 33), (14, 13), (2, 8), (3, 10), (16, 17), (26, 35)], 
			[(25, 3), (14, 16), (2, 26), (3, 25), (16, 14), (26, 2)]
	), 
	16: (
			[26, 15, 3, 4, 17, 27], 
			[(26, 34), (15, 14), (3, 9), (4, 11), (17, 18), (27, 36)], 
			[(26, 4), (15, 17), (3, 27), (4, 26), (17, 15), (27, 3)]
	), 
	17: (
			[27, 16, 4, 5, 18, 28], 
			[(27, 35), (16, 15), (4, 10), (5, 12)], 
			[(27, 5), (16, 18), (4, 28), (5, 27), (18, 16), (28, 4)]
	), 
	18: (
			[28, 17, 5, 6], 
			[(28, 36), (17, 16), (5, 11)], 
			[(28, 6), (6, 28)]
	), 
	0: (
			[7, 1, 13], 
			[(7, 19), (1, 2), (13, 24)], 
			[]
	), 
	1: (
			[13, 0, 7, 8, 2, 14], 
			[(8, 20), (2, 3), (14, 25)], 
			[(13, 8), (0, 2), (7, 14), (8, 13), (2, 0), (14, 7)]
	), 
	2: (
			[14, 1, 8, 9, 3, 15], 
			[(14, 24), (1, 0), (8, 19), (9, 21), (3, 4), (15, 26)], 
			[(14, 9), (1, 3), (8, 15), (9, 14), (3, 1), (15, 8)]
	), 
	3: (
			[15, 2, 9, 10, 4, 16], 
			[(15, 25), (2, 1), (9, 20), (10, 22), (4, 5), (16, 27)], 
			[(15, 10), (2, 4), (9, 16), (10, 15), (4, 2), (16, 9)]
	), 
	4: (
			[16, 3, 10, 11, 5, 17], 
			[(16, 26), (3, 2), (10, 21), (11, 23), (5, 6), (17, 28)], 
			[(16, 11), (3, 5), (10, 17), (11, 16), (5, 3), (17, 10)]
	), 
	5: (
			[17, 4, 11, 12, 6, 18], 
			[(17, 27), (4, 3), (11, 22)], 
			[(17, 12), (4, 6), (11, 18), (12, 17), (6, 4), (18, 11)]
	), 
	6: (
			[18, 5, 12], 
			[(18, 28), (5, 4), (12, 23)], 
			[]
	), 
	7: (
			[0, 19, 8, 1], 
			[(19, 29), (8, 9), (1, 14)], 
			[(0, 19), (19, 0)]
	), 
	8: (
			[1, 7, 19, 20, 9, 2], 
			[(1, 13), (20, 30), (9, 10), (2, 15)], 
			[(1, 20), (7, 9), (19, 2), (20, 1), (9, 7), (2, 19)]
	), 
	9: (
			[2, 8, 20, 21, 10, 3], 
			[(2, 14), (8, 7), (20, 29), (21, 31), (10, 11), (3, 16)], 
			[(2, 21), (8, 10), (20, 3), (21, 2), (10, 8), (3, 20)]
	), 
	10: (
			[3, 9, 21, 22, 11, 4], 
			[(3, 15), (9, 8), (21, 30), (22, 32), (11, 12), (4, 17)], 
			[(3, 22), (9, 11), (21, 4), (22, 3), (11, 9), (4, 21)]
	), 
	11: (
			[4, 10, 22, 23, 12, 5], 
			[(4, 16), (10, 9), (22, 31), (5, 18)], 
			[(4, 23), (10, 12), (22, 5), (23, 4), (12, 10), (5, 22)]
	), 
	12: (
			[5, 11, 23, 6], 
			[(5, 17), (11, 10), (23, 32)], 
			[(23, 6), (6, 23)]
	), 
	19: (
			[7, 29, 20, 8], 
			[(7, 0), (20, 21), (8, 2)], 
			[(7, 29), (29, 7)]
	), 
	20: (
			[8, 19, 29, 30, 21, 9], 
			[(8, 1), (21, 22), (9, 3)], 
			[(8, 30), (19, 21), (29, 9), (30, 8), (21, 19), (9, 29)]
	), 
	21: (
			[9, 20, 30, 31, 22, 10], 
			[(9, 2), (20, 19), (22, 23), (10, 4)], 
			[(9, 31), (20, 22), (30, 10), (31, 9), (22, 20), (10, 30)]
	), 
	22: (
			[10, 21, 31, 32, 23, 11], 
			[(10, 3), (21, 20), (11, 5)], 
			[(10, 32), (21, 23), (31, 11), (32, 10), (23, 21), (11, 31)]
	), 
	23: (
			[11, 22, 32, 12], 
			[(11, 4), (22, 21), (12, 6)], 
			[(32, 12), (12, 32)]
	), 
	29: (
			[19, 30, 20], 
			[(19, 7), (30, 31), (20, 9)], 
			[]
	), 
	30: (
			[20, 29, 31, 21], 
			[(20, 8), (31, 32), (21, 10)], 
			[(29, 31), (31, 29)]
	), 
	31: (
			[21, 30, 32, 22], 
			[(21, 9), (30, 29), (22, 11)], 
			[(30, 32), (32, 30)]
	), 
	32: (
			[22, 31, 23], 
			[(22, 10), (31, 30), (23, 12)], 
			[]
	)
}

# No. 2 - Available Moves (possibleDict)
## How to Generate:
# _ADJACENT_STEPS = [(-1,+0),(+0,-1),(+1,-1),(+1,+0),(+0,+1),(-1,+1)]
# _BOARD_RANGE = range(-3,4)
# _BOARD_AREA = [(q,r) for q in _BOARD_RANGE for r in _BOARD_RANGE if -q-r in _BOARD_RANGE]
# b = {}
# f = [30,22,12,0,7,19,29,31]
# hsh = lambda a,b: f[a+3]+b+3
# for (q, r) in _BOARD_AREA:
#     tiles = []
#     for (x, y) in _ADJACENT_STEPS:
#         if ((q+x, r+y)) in _BOARD_AREA:
#             tiles.append(hsh(q+x, r+y))
#         if ((q+x*2, r+y*2)) in _BOARD_AREA:
#             tiles.append(hsh(q+x*2, r+y*2))
#     b[hsh(q, r)] = tiles

# I then manually added the exit tiles (4, 4) to every exit tile for every player
"""
## Description:

This dictionary returns the theoretically possible moves for a given tile.

For example, tile (0, 0) has possible moves:
[
	(-1, 0), 
	(-2, 0), 
	(0, -1), 
	(0, -2), 
	(1, -1), 
	(2, -2), 
	(1, 0), 
	(2, 0), 
	(0, 1), 
	(0, 2), 
	(-1, 1), 
	(-2, 2)
], 

Moves are only stored as a single (destination) tuple, since the source is the
queried piece.
"""
possibleDict = {
	0: [7, 19, 1, 2, 13, 24, 38], 
	1: [13, 0, 7, 8, 20, 2, 3, 14, 25], 
	2: [14, 24, 1, 0, 8, 19, 9, 21, 3, 4, 15, 26], 
	3: [15, 25, 2, 1, 9, 20, 10, 22, 4, 5, 16, 27], 
	4: [16, 26, 3, 2, 10, 21, 11, 23, 5, 6, 17, 28], 
	5: [17, 27, 4, 3, 11, 22, 12, 6, 18], 
	6: [18, 28, 5, 4, 12, 23, 38], 
	7: [0, 19, 29, 8, 9, 1, 14], 
	8: [1, 13, 7, 19, 20, 30, 9, 10, 2, 15], 
	9: [2, 14, 8, 7, 20, 29, 21, 31, 10, 11, 3, 16], 
	10: [3, 15, 9, 8, 21, 30, 22, 32, 11, 12, 4, 17], 
	11: [4, 16, 10, 9, 22, 31, 23, 12, 5, 18], 
	12: [5, 17, 11, 10, 23, 32, 6], 
	13: [0, 1, 8, 14, 15, 24, 33, 38], 
	14: [24, 13, 1, 7, 2, 9, 15, 16, 25, 34], 
	15: [25, 33, 14, 13, 2, 8, 3, 10, 16, 17, 26, 35], 
	16: [26, 34, 15, 14, 3, 9, 4, 11, 17, 18, 27, 36], 
	17: [27, 35, 16, 15, 4, 10, 5, 12, 18, 28], 
	18: [28, 36, 17, 16, 5, 11, 6, 38], 
	19: [7, 0, 29, 20, 21, 8, 2], 
	20: [8, 1, 19, 29, 30, 21, 22, 9, 3], 
	21: [9, 2, 20, 19, 30, 31, 22, 23, 10, 4], 
	22: [10, 3, 21, 20, 31, 32, 23, 11, 5], 
	23: [11, 4, 22, 21, 32, 12, 6], 
	24: [13, 0, 14, 2, 25, 26, 33, 38], 
	25: [33, 24, 14, 1, 15, 3, 26, 27, 34], 
	26: [34, 25, 24, 15, 2, 16, 4, 27, 28, 35], 
	27: [35, 26, 25, 16, 3, 17, 5, 28, 36], 
	28: [36, 27, 26, 17, 4, 18, 6, 38], 
	29: [19, 7, 30, 31, 20, 9, 38], 
	30: [20, 8, 29, 31, 32, 21, 10, 38], 
	31: [21, 9, 30, 29, 32, 22, 11, 38], 
	32: [22, 10, 31, 30, 23, 12, 38],
	33: [24, 13, 25, 15, 34, 35, 38], 
	34: [33, 25, 14, 26, 16, 35, 36], 
	35: [34, 33, 26, 15, 27, 17, 36], 
	36: [35, 34, 27, 16, 28, 18, 38]
}

# No. 3 - Tile Location (occupied)

# How to Generate
# occupied = [False] * 37

"""
Description:

This dictionary determines which tiles are occupied in the board. 

The hash function 

f = [30,22,12,0,7,19,29]
hsh = lambda a,b: f[a+3]+b+3
determines the position of the board tile in this list.
"""
occupied = [
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False, 
	False
]

# No. 4 - Move Validity (validDict)
# ===== THIS DICTIONARY IS NEARLY 500 LINES =====
# Beautified because otherwise it's ugly.
# (It'll be compiled anyway so it doesn't really matter)

# How to Generate
# temp = [{}] * 37
# f = [30,22,12,0,7,19,29,31]
# hsh = lambda a,b: f[a+3]+b+3
# hd = lambda a, b: (abs(a[0]-b[0]) + abs(a[0]+a[1]-b[0]-b[1]) + abs(a[1]-b[1]))/2
# for a in possibleDict:
# 	temp2 = {}
# 	for b in possibleDict[a]:
# 		# Set all of the 'MOVE' actions as available for simplicity
# 		temp2[b] = True
# 		if hd(reverse_hash[a],reverse_hash[b]) > 1:
# 			temp2[b] = False
# 	temp[a] = temp2
"""
Description:

This dictionary is the availability of every possible 
move from and to every possible tile.
stored as (to, from)
"""

validDict = [
	{
		7: True, 
		19: False, 
		1: True, 
		2: False, 
		13: True, 
		24: False
	},
	{
		13: True, 
		0: True, 
		7: True, 
		8: True, 
		20: False, 
		2: True, 
		3: False, 
		14: True, 
		25: False
	},
	{
		14: True, 
		24: False, 
		1: True, 
		0: False, 
		8: True, 
		19: False, 
		9: True, 
		21: False, 
		3: True, 
		4: False, 
		15: True, 
		26: False
	},
	{
		15: True, 
		25: False, 
		2: True, 
		1: False, 
		9: True, 
		20: False, 
		10: True, 
		22: False, 
		4: True, 
		5: False, 
		16: True, 
		27: False
	},
	{
		16: True, 
		26: False, 
		3: True, 
		2: False, 
		10: True, 
		21: False, 
		11: True, 
		23: False, 
		5: True, 
		6: False, 
		17: True, 
		28: False
	},
	{
		17: True, 
		27: False, 
		4: True, 
		3: False, 
		11: True, 
		22: False, 
		12: True, 
		6: True, 
		18: True
	},
	{
		18: True, 
		28: False, 
		5: True, 
		4: False, 
		12: True, 
		23: False
	},
	{
		0: True, 
		19: True, 
		29: False, 
		8: True, 
		9: False, 
		1: True, 
		14: False
	},
	{
		1: True, 
		13: False, 
		7: True, 
		19: True, 
		20: True, 
		30: False, 
		9: True, 
		10: False, 
		2: True, 
		15: False
	},
	{
		2: True, 
		14: False, 
		8: True, 
		7: False, 
		20: True, 
		29: False, 
		21: True, 
		31: False, 
		10: True, 
		11: False, 
		3: True, 
		16: False
	},
	{
		3: True, 
		15: False, 
		9: True, 
		8: False, 
		21: True, 
		30: False, 
		22: True, 
		32: False, 
		11: True, 
		12: False, 
		4: True, 
		17: False
	},
	{
		4: True, 
		16: False, 
		10: True, 
		9: False, 
		22: True, 
		31: False, 
		23: True, 
		12: True, 
		5: True, 
		18: False
	},
	{
		5: True, 
		17: False, 
		11: True, 
		10: False, 
		23: True, 
		32: False, 
		6: True
	},
	{
		0: True, 
		1: True, 
		8: False, 
		14: True, 
		15: False, 
		24: True, 
		33: False
	},
	{
		24: True, 
		13: True, 
		1: True, 
		7: False, 
		2: True, 
		9: False, 
		15: True, 
		16: False, 
		25: True, 
		34: False
	},
	{
		25: True, 
		33: False, 
		14: True, 
		13: False, 
		2: True, 
		8: False, 
		3: True, 
		10: False, 
		16: True, 
		17: False, 
		26: True, 
		35: False
	},
	{
		26: True, 
		34: False, 
		15: True, 
		14: False, 
		3: True, 
		9: False, 
		4: True, 
		11: False, 
		17: True, 
		18: False, 
		27: True, 
		36: False
	},
	{
		27: True, 
		35: False, 
		16: True, 
		15: False, 
		4: True, 
		10: False, 
		5: True, 
		12: False, 
		18: True, 
		28: True
	},
	{
		28: True, 
		36: False, 
		17: True, 
		16: False, 
		5: True, 
		11: False, 
		6: True
	},
	{
		7: True, 
		0: False, 
		29: True, 
		20: True, 
		21: False, 
		8: True, 
		2: False
	},
	{
		8: True, 
		1: False, 
		19: True, 
		29: True, 
		30: True, 
		21: True, 
		22: False, 
		9: True, 
		3: False
	},
	{
		9: True, 
		2: False, 
		20: True, 
		19: False, 
		30: True, 
		31: True, 
		22: True, 
		23: False, 
		10: True, 
		4: False
	},
	{
		10: True, 
		3: False, 
		21: True, 
		20: False, 
		31: True, 
		32: True, 
		23: True, 
		11: True, 
		5: False
	},
	{
		11: True, 
		4: False, 
		22: True, 
		21: False, 
		32: True, 
		12: True, 
		6: False
	},
	{
		13: True, 
		0: False, 
		14: True, 
		2: False, 
		25: True, 
		26: False, 
		33: True
	},
	{
		33: True, 
		24: True, 
		14: True, 
		1: False, 
		15: True, 
		3: False, 
		26: True, 
		27: False, 
		34: True
	},
	{
		34: True, 
		25: True, 
		24: False, 
		15: True, 
		2: False, 
		16: True, 
		4: False, 
		27: True, 
		28: False, 
		35: True
	},
	{
		35: True, 
		26: True, 
		25: False, 
		16: True, 
		3: False, 
		17: True, 
		5: False, 
		28: True, 
		36: True
	},
	{
		36: True, 
		27: True, 
		26: False, 
		17: True, 
		4: False, 
		18: True, 
		6: False
	},
	{
		19: True, 
		7: False, 
		30: True, 
		31: False, 
		20: True, 
		9: False
	},
	{
		20: True, 
		8: False, 
		29: True, 
		31: True, 
		32: False, 
		21: True, 
		10: False
	},
	{
		21: True, 
		9: False, 
		30: True, 
		29: False, 
		32: True, 
		22: True, 
		11: False
	},
	{
		22: True, 
		10: False, 
		31: True, 
		30: False, 
		23: True, 
		12: False
	},
	{
		24: True, 
		13: False, 
		25: True, 
		15: False, 
		34: True, 
		35: False
	},
	{
		33: True, 
		25: True, 
		14: False, 
		26: True, 
		16: False, 
		35: True, 
		36: False
	},
	{
		34: True, 
		33: False, 
		26: True, 
		15: False, 
		27: True, 
		17: False, 
		36: True
	},
	{
		35: True, 
		34: False, 
		27: True, 
		16: False, 
		28: True, 
		18: False
	}
]

# Dict 5: Reverse hash
# For the few times we actually need to see a piece.
reverse_hash = {
	0: (0, -3),
	1: (0, -2),
	2: (0, -1),
	3: (0, 0),
	4: (0, 1),
	5: (0, 2),
	6: (0, 3),
	7: (1, -3),
	8: (1, -2),
	9: (1, -1),
	10: (1, 0),
	11: (1, 1),
	12: (1, 2),
	13: (-1, -2),
	14: (-1, -1),
	15: (-1, 0),
	16: (-1, 1),
	17: (-1, 2),
	18: (-1, 3),
	19: (2, -3),
	20: (2, -2),
	21: (2, -1),
	22: (2, 0),
	23: (2, 1),
	24: (-2, -1),
	25: (-2, 0),
	26: (-2, 1),
	27: (-2, 2),
	28: (-2, 3),
	29: (3, -3),
	30: (3, -2),
	31: (3, -1),
	32: (3, 0),
	33: (-3, 0),
	34: (-3, 1),
	35: (-3, 2),
	36: (-3, 3),
	38: (4, 4)
}

owned_by = {
	0: -1,
	1: -1,
	2: -1,
	3: -1,
	4: -1,
	5: -1,
	6: -1,
	7: -1,
	8: -1,
	9: -1,
	10: -1,
	11: -1,
	12: -1,
	13: -1,
	14: -1,
	15: -1,
	16: -1,
	17: -1,
	18: -1,
	19: -1,
	20: -1,
	21: -1,
	22: -1,
	23: -1,
	24: -1,
	25: -1,
	26: -1,
	27: -1,
	28: -1,
	29: -1,
	30: -1,
	31: -1,
	32: -1,
	33: -1,
	34: -1,
	35: -1,
	36: -1,
}

# f = [30,22,12,0,7,19,29,31]
# hsh2 = lambda a: f[a[0]+3]+a[1]+3
# temp = {}
# for x in possibleDict:
# 	temp2 = {}
# 	for i in possibleDict[x]:
# 		rh_pce = reverse_hash[x]
# 		rh_possible = reverse_hash[i]
# 		jump = False
# 		jp = 38
# 		d = (abs(rh_pce[0]-rh_possible[0])
# 		 + abs(rh_pce[0]+rh_pce[1]-rh_possible[0]-rh_possible[1]) 
# 		 + abs(rh_pce[1]-rh_possible[1]))/2
# 		# is it a jump?
# 		if d > 1:
# 			jp = hsh2( (int((rh_pce[0]+rh_possible[0])/2),int((rh_pce[1]+rh_possible[1])/2)) )
# 			jump = True
# 		tup = (jump, jp)
# 		temp2[i] = (tup)
# 	temp[x] = temp2


jump_dict = {
	0: {
		7: (False, 38), 
		19: (True, 7), 
		1: (False, 38), 
		2: (True, 1), 
		13: (False, 38), 
		24: (True, 13)
	}, 
	1: {
		13: (False, 38), 
		0: (False, 38), 
		7: (False, 38), 
		8: (False, 38), 
		20: (True, 8), 
		2: (False, 38), 
		3: (True, 2), 
		14: (False, 38), 
		25: (True, 14)
	}, 
	2: {
		14: (False, 38), 
		24: (True, 14), 
		1: (False, 38), 
		0: (True, 1), 
		8: (False, 38), 
		19: (True, 8), 
		9: (False, 38), 
		21: (True, 9), 
		3: (False, 38), 
		4: (True, 3), 
		15: (False, 38), 
		26: (True, 15)
	}, 
	3: {
		15: (False, 38), 
		25: (True, 15), 
		2: (False, 38), 
		1: (True, 2), 
		9: (False, 38), 
		20: (True, 9), 
		10: (False, 38), 
		22: (True, 10), 
		4: (False, 38), 
		5: (True, 4), 
		16: (False, 38), 
		27: (True, 16)
	}, 
	4: {
		16: (False, 38), 
		26: (True, 16), 
		3: (False, 38), 
		2: (True, 3), 
		10: (False, 38), 
		21: (True, 10), 
		11: (False, 38), 
		23: (True, 11), 
		5: (False, 38), 
		6: (True, 5), 
		17: (False, 38), 
		28: (True, 17)
	}, 
	5: {
		17: (False, 38), 
		27: (True, 17), 
		4: (False, 38), 
		3: (True, 4), 
		11: (False, 38), 
		22: (True, 11), 
		12: (False, 38), 
		6: (False, 38), 
		18: (False, 38)
	}, 
	6: {
		18: (False, 38), 
		28: (True, 18), 
		5: (False, 38), 
		4: (True, 5), 
		12: (False, 38), 
		23: (True, 12)
	}, 
	7: {
		0: (False, 38), 
		19: (False, 38), 
		29: (True, 19), 
		8: (False, 38), 
		9: (True, 8), 
		1: (False, 38), 
		14: (True, 1)
	}, 
	8: {
		1: (False, 38), 
		13: (True, 1), 
		7: (False, 38), 
		19: (False, 38), 
		20: (False, 38), 
		30: (True, 20), 
		9: (False, 38), 
		10: (True, 9), 
		2: (False, 38), 
		15: (True, 2)
	}, 
	9: {
		2: (False, 38), 
		14: (True, 2), 
		8: (False, 38), 
		7: (True, 8), 
		20: (False, 38), 
		29: (True, 20), 
		21: (False, 38), 
		31: (True, 21), 
		10: (False, 38), 
		11: (True, 10), 
		3: (False, 38), 
		16: (True, 3)
	}, 
	10: {
		3: (False, 38), 
		15: (True, 3), 
		9: (False, 38), 
		8: (True, 9), 
		21: (False, 38), 
		30: (True, 21), 
		22: (False, 38), 
		32: (True, 22), 
		11: (False, 38), 
		12: (True, 11), 
		4: (False, 38), 
		17: (True, 4)
	}, 
	11: {
		4: (False, 38), 
		16: (True, 4), 
		10: (False, 38), 
		9: (True, 10), 
		22: (False, 38), 
		31: (True, 22), 
		23: (False, 38), 
		12: (False, 38), 
		5: (False, 38), 
		18: (True, 5)
	}, 
	12: {
		5: (False, 38), 
		17: (True, 5), 
		11: (False, 38), 
		10: (True, 11), 
		23: (False, 38), 
		32: (True, 23), 
		6: (False, 38)
	}, 
	13: {
		0: (False, 38), 
		1: (False, 38), 
		8: (True, 1), 
		14: (False, 38), 
		15: (True, 14), 
		24: (False, 38), 
		33: (True, 24)
	}, 
	14: {
		24: (False, 38), 
		13: (False, 38), 
		1: (False, 38), 
		7: (True, 1), 
		2: (False, 38), 
		9: (True, 2), 
		15: (False, 38), 
		16: (True, 15), 
		25: (False, 38), 
		34: (True, 25)
	}, 
	15: {
		25: (False, 38), 
		33: (True, 25), 
		14: (False, 38), 
		13: (True, 14), 
		2: (False, 38), 
		8: (True, 2), 
		3: (False, 38), 
		10: (True, 3), 
		16: (False, 38), 
		17: (True, 16), 
		26: (False, 38), 
		35: (True, 26)
	}, 
	16: {
		26: (False, 38), 
		34: (True, 26), 
		15: (False, 38), 
		14: (True, 15), 
		3: (False, 38), 
		9: (True, 3), 
		4: (False, 38), 
		11: (True, 4), 
		17: (False, 38), 
		18: (True, 17), 
		27: (False, 38), 
		36: (True, 27)
	}, 
	17: {
		27: (False, 38), 
		35: (True, 27), 
		16: (False, 38), 
		15: (True, 16), 
		4: (False, 38), 
		10: (True, 4), 
		5: (False, 38), 
		12: (True, 5), 
		18: (False, 38), 
		28: (False, 38)
	}, 
	18: {
		28: (False, 38), 
		36: (True, 28), 
		17: (False, 38), 
		16: (True, 17), 
		5: (False, 38), 
		11: (True, 5), 
		6: (False, 38)
	}, 
	19: {
		7: (False, 38), 
		0: (True, 7), 
		29: (False, 38), 
		20: (False, 38), 
		21: (True, 20), 
		8: (False, 38), 
		2: (True, 8)
	}, 
	20: {
		8: (False, 38), 
		1: (True, 8), 
		19: (False, 38), 
		29: (False, 38), 
		30: (False, 38), 
		21: (False, 38), 
		22: (True, 21), 
		9: (False, 38), 
		3: (True, 9)
	}, 
	21: {
		9: (False, 38), 
		2: (True, 9), 
		20: (False, 38), 
		19: (True, 20), 
		30: (False, 38), 
		31: (False, 38), 
		22: (False, 38), 
		23: (True, 22), 
		10: (False, 38), 
		4: (True, 10)
	}, 
	22: {
		10: (False, 38), 
		3: (True, 10), 
		21: (False, 38), 
		20: (True, 21), 
		31: (False, 38), 
		32: (False, 38), 
		23: (False, 38), 
		11: (False, 38), 
		5: (True, 11)
	}, 
	23: {
		11: (False, 38), 
		4: (True, 11), 
		22: (False, 38), 
		21: (True, 22), 
		32: (False, 38), 
		12: (False, 38), 
		6: (True, 12)
	}, 
	24: {
		13: (False, 38), 
		0: (True, 13), 
		14: (False, 38), 
		2: (True, 14), 
		25: (False, 38), 
		26: (True, 25), 
		33: (False, 38)
	}, 
	25: {
		33: (False, 38), 
		24: (False, 38), 
		14: (False, 38), 
		1: (True, 14), 
		15: (False, 38), 
		3: (True, 15), 
		26: (False, 38), 
		27: (True, 26), 
		34: (False, 38)
	}, 
	26: {
		34: (False, 38), 
		25: (False, 38), 
		24: (True, 25), 
		15: (False, 38), 
		2: (True, 15), 
		16: (False, 38), 
		4: (True, 16), 
		27: (False, 38), 
		28: (True, 27), 
		35: (False, 38)
	}, 
	27: {
		35: (False, 38), 
		26: (False, 38), 
		25: (True, 26), 
		16: (False, 38), 
		3: (True, 16), 
		17: (False, 38), 
		5: (True, 17), 
		28: (False, 38), 
		36: (False, 38)
	}, 
	28: {
		36: (False, 38), 
		27: (False, 38), 
		26: (True, 27), 
		17: (False, 38), 
		4: (True, 17), 
		18: (False, 38), 
		6: (True, 18)
	}, 
	29: {
		19: (False, 38), 
		7: (True, 19), 
		30: (False, 38), 
		31: (True, 30), 
		20: (False, 38), 
		9: (True, 20)
	}, 
	30: {
		20: (False, 38), 
		8: (True, 20), 
		29: (False, 38), 
		31: (False, 38), 
		32: (True, 31), 
		21: (False, 38), 
		10: (True, 21)
	}, 
	31: {
		21: (False, 38), 
		9: (True, 21), 
		30: (False, 38), 
		29: (True, 30), 
		32: (False, 38), 
		22: (False, 38), 
		11: (True, 22)
	}, 
	32: {
		22: (False, 38), 
		10: (True, 22), 
		31: (False, 38), 
		30: (True, 31), 
		23: (False, 38), 
		12: (True, 23)
	}, 
	33: {
		24: (False, 38), 
		13: (True, 24), 
		25: (False, 38), 
		15: (True, 25), 
		34: (False, 38), 
		35: (True, 34)
	}, 
	34: {
		33: (False, 38), 
		25: (False, 38), 
		14: (True, 25), 
		26: (False, 38), 
		16: (True, 26), 
		35: (False, 38), 
		36: (True, 35)
	}, 
	35: {
		34: (False, 38), 
		33: (True, 34), 
		26: (False, 38), 
		15: (True, 26), 
		27: (False, 38), 
		17: (True, 27), 
		36: (False, 38)
	}, 
	36: {
		35: (False, 38), 
		34: (True, 35), 
		27: (False, 38), 
		16: (True, 27), 
		28: (False, 38), 
		18: (True, 28)
	}
}